﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public static GameController instance;

    bool playerAlive;
    bool startMovingWorld;

    int lastIndex;

    public bool Alive { get => playerAlive; }
    public bool MoveWorld { get => startMovingWorld; }

    public List<FloorSpawner> floorSpawners = new List<FloorSpawner>();

    public Button readyButton;

    float floorSpawnTimer;
    public float floorSpawnDelay = 2.0f;

    int triggersHit;

    private void Start()
    {
        if(instance != null)
        {
            instance = null;
        }
        instance = this;

        playerAlive = true;
        startMovingWorld = false;

        GameOverCollider.gameOver += GameOverCalled;
        triggersHit = 0;
    }

    public void StartGame()
    {
        startMovingWorld = true;
        readyButton.gameObject.SetActive(false);
    }

    void GameOverCalled()
    {
        startMovingWorld = false;
        playerAlive = false;
    }

    private void Update()
    {
        if(startMovingWorld && playerAlive)
        {
            if (triggersHit > 1)
            {
                System.Random r = new System.Random();

                int index = r.Next() % floorSpawners.Count;
                while (index == lastIndex)
                {
                    index = r.Next() % floorSpawners.Count;
                }
                floorSpawners[index].SpawnFloor();
                floorSpawnTimer = 0.0f;
                lastIndex = index;
                triggersHit = 1;
            }
        }
    }

    public void IncreaseTriggersHit()
    {
        triggersHit++;
    }
}
