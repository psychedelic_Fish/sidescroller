﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] bool grounded; //is the player touching the ground or not
    [SerializeField] bool isSmall;  //is the player at full height or half height
    public float intialJumpSpeed; //how fast should the player leave the ground

    public float jumpDecelleration; //how quickly the player will fall to the ground
    Vector2 jumpSpeed;              //The actual vector for moving the player up and down in the jump

    void Start()
    {
        grounded = false; //intially set grounded to be true, as the player will start on the ground
        jumpSpeed = new Vector2(0, 0); //intialise jump velocity to 0
        isSmall = false;
    }

    // Update is called once per frame
    void Update()
    {
        //First check if the world is moving and the player is alive
        if (GameController.instance.MoveWorld && GameController.instance.Alive) 
        {
            /*If the player uses the key "Touch" set up in ProjectSettings/Editor/Inputs
              and the player is touching the ground*/
            if (Input.GetButtonDown("Touch") && grounded && !isSmall)
            {
                //Calculate our intial jump speed using the up Vector supplied to us by Unity, an intial jump speed
                jumpSpeed = Vector2.up * intialJumpSpeed;
            }
            if (Input.GetButtonDown("Shrink"))
            {
                ChangePlayerSize();
            }
            //Grab a reference to our players position, Vector2 as we are uninterested in the z
            var temp = (Vector2)transform.position;
            //Apply our jump speed to the reference, making sure to multiply by deltaTime
            temp += jumpSpeed * Time.deltaTime;
            //Apply our altered position to our objects actual position
            transform.position = temp;

            //If the player is in the air
            if (!grounded)
            {
                //Adjust our jumpspeed by a amount on the down vector so as to slow out assent and begin our dissent
                jumpSpeed += Vector2.down * jumpDecelleration * Time.deltaTime;
            }
        }
    }


    
    /* this will be called when the player hits the ground
     * this will set grounded to true*/
    private void OnTriggerEnter(Collider other)
    {
        grounded = true;
        jumpSpeed *= 0; //Player has hit the ground so reset jumpSpeed to 0
    }
    /* this will be called when the player leaves the ground
 * this will set grounded to false*/
    private void OnTriggerExit(Collider other)
    {
        grounded = false;
    }

    void ChangePlayerSize()
    {
        var temp = transform.localScale;
        if (isSmall)
        {
            temp.y /= 0.5f;
        }
        else
        {
            temp.y *= 0.5f;
        }
        transform.localScale = temp;
        isSmall = !(isSmall);
    }
}
