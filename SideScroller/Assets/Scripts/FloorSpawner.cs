﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorSpawner : MonoBehaviour
{
    public GameObject objectToSpawn;

    public void SpawnFloor()
    {
        Instantiate(objectToSpawn, transform.position, Quaternion.identity);
    }

}
