﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverCollider : MonoBehaviour
{
    public delegate void GameOver();
    public static event GameOver gameOver;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            gameOver();
        }
        else
        {
            return;
        }
    }
}
