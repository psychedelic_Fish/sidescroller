﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{
    public float moveSpeed; //How fast objects in the world will move
    
    // Update is called once per frame
    void Update()
    {
        if (GameController.instance.MoveWorld)
        {
            var temp = (Vector2)transform.position; //Grab a reference to the objects position
            temp += Vector2.left * moveSpeed * Time.deltaTime; //Apply our moveSpeed in the direction of left of screen
            transform.position = temp; //Apply our new position to the objects position
        }
    }
    private void OnBecameInvisible()
    {
        Destroy(gameObject);
        Debug.Log("Destroy");
    }

}
