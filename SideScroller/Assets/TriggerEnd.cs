﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerEnd : MonoBehaviour
{
    bool isColliding;
    private void OnTriggerExit(Collider other)
    {
        if (isColliding)
        {
            return;
        }
        if (other.gameObject.CompareTag("Player"))
        {
            isColliding = true;
            GameController.instance.IncreaseTriggersHit();
        }
    }

    private void Update()
    {
        isColliding = false;
    }
}
